FROM php:7-fpm
RUN apt-get update && apt-get install -y \
    libmcrypt-dev \
    git \
    unzip \
    zlib1g-dev

RUN apt-get install -y php5-memcached
RUN docker-php-ext-install mcrypt mbstring tokenizer mysqli pdo pdo_mysql zip

WORKDIR /var/www/html/app

RUN curl -sS https://getcomposer.org/installer | php -- \
       --install-dir=/usr/local/bin \
       --filename=composer
