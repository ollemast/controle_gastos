# Projeto Controle de Gastos Dockerizado em PHP

### Requisitos da máquina do desenvolvedor:
```
Docker
Docker Compose
```

## Modo de usar
### Após o clone do projeto, executar o docker Compose
```
$ git clone git@bitbucket.org:ollemast/controle_gastos.git
$ docker-compose up -d
```
